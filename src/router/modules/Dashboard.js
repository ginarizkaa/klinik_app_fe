const Dashboard = [
	{
		path: "/dashboard",
		component: () => import("@/views"),
		children: [
			{
				path: "dashboard.v1",
				name: "dashboard.v1",
				component: () => import("@/views/Dashboard/Dashboard.vue"),
			},
			{
				path: "dashboard.v2",
				name: "dashboard.v2",
				component: () => import("@/views/Dashboard/Dashboard2.vue"),
			},
		],
	},
]

export default Dashboard
