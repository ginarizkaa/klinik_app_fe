const Settings = [
	{
		path: "/settings",
		component: () => import("@/views"),
		children: [
			{
				path: "menu",
				name: "menu",
				component: () => import("@/views/Settings/Menus/Menu.vue"),
			},
			{
				path: "roles",
				name: "roles",
				component: () => import("@/views/Settings/Routes/Route.vue"),
			},
			{
				path: "routes",
				name: "routes",
				component: () => import("@/views/Settings/Routes/Route.vue"),
			},
		],
	},
]

export default Settings
