import Vue from "vue"
import App from "./App.vue"
import "./registerServiceWorker"
import router from "./router"
import store from "./store/index"
import vuetify from "./plugins/vuetify"
import "./assets/css/main.scss"

// ----- Validation Form
import VeeValidate from "vee-validate"
Vue.use(VeeValidate)

// ----- Mobile Detection
import VueMobileDetection from "vue-mobile-detection"
Vue.use(VueMobileDetection)

// ----- Axios
import "./library/axios"

// ----- Vue-Easytable
import VueEasytable from "vue-easytable"
import "vue-easytable/libs/theme-default/index.css"
Vue.use(VueEasytable)

// ----- Components
import Breadcumb from "@/components/Breadcumb"
Vue.component("breadcumb", Breadcumb)

Vue.config.productionTip = false

new Vue({
	store,
	router,
	vuetify,
	render: (h) => h(App),
}).$mount("#app")
