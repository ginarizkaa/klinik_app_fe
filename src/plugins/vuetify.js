import Vue from "vue"
import Vuetify from "vuetify/lib/framework"
// import { CUSTOM_ICONS } from "./fontAwesome"

Vue.use(Vuetify)

/*
THEME COLORS
> primary
> secondary
> accent
> error
> info
> success
> warning
*/

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: "#2B92E4",
				secondary: "#2b3f6c",
				error: "#F7444E",
				info: "#C0EB6A",
				// warning: "#FED361"
			},
		},
	},
	// icons: {
	// 	iconfont: "faSvg",
	// 	values: CUSTOM_ICONS,
	// },
})
