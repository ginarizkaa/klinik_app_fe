import Vue from "vue"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
import { library } from "@fortawesome/fontawesome-svg-core"
import { fas } from "@fortawesome/free-solid-svg-icons"

Vue.component("font-awesome-icon", FontAwesomeIcon)
library.add(fas)

let fasArray = Object.keys(library.definitions.fas)

let CUSTOM_ICONS = {}
fasArray.forEach((element) => {
	// eslint-disable-next-line no-unused-vars
	let item_name = element.replace("-", "_")
	Object.assign(CUSTOM_ICONS, {
		[item_name]: {
			component: FontAwesomeIcon,
			props: {
				icon: ["fas", element],
			},
		},
	})
})

export { CUSTOM_ICONS }
