import Vue from "vue"
import axios from "axios"
import store from "../store"

let url = process.env.VUE_APP_URL_API
let timeout = 60000

const getToken = () => {
	const token = store.getters["SYSTEM/getAccessToken"] !== null ? "Bearer " + store.getters["SYSTEM/getAccessToken"] : ""
	return token
}

const base = axios.create({
	baseURL: url,
	timeout: timeout,
	headers: {
		"Access-Control-Allow-Origin": "*",
		"Content-Type": "application/json",
		Accept: "application/json",
		Authorization: getToken(),
	},
})

base.interceptors.response.use(
	(response) => {
		if (response.data.code == "401") {
			// Unauthorized.
			localStorage.removeItem("vuex")
			window.location = "/"
			return
		}
		return response
	},
	(error) => {
		console.log("error", error.response)
		let data = {
			code: 500,
			status: "ERROR",
			message: error.message || "Internal Server Error.",
			data: null,
		}
		if (error.response && error.response.data) {
			let res = error.response
			data = {
				code: res.status,
				status: "ERROR",
				message: res.statusText,
				data: res.data,
			}
			return Promise.reject(data)
		}
		return Promise.reject(data)
	},
)

Vue.prototype.$http = base
