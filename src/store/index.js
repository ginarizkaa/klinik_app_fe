import createPersistedState from "vuex-persistedstate"
import Vue from "vue"
import Vuex from "vuex"

import SYSTEM from "./SYSTEM/index"

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		SYSTEM,
	},
	plugins: [createPersistedState()],
})

export default store
