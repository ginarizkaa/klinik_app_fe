export default {
	setDataUser(state, data) {
		state.dataUser = data
	},
	setDataMenus(state, data) {
		state.dataMenus = data
	},
	setAccessPage(state, data) {
		state.accessPage = data
	},
	setAccessToken(state, data) {
		state.accessToken = data
	},
	setDefaultMenu(state, data) {
		state.defaultMenu = data
	},
	setCurrentMenuId(state, data) {
		state.currentMenuId = data
	},
	setCurrentMenuParentId(state, data) {
		state.currentMenuParentId = data
	},
}
