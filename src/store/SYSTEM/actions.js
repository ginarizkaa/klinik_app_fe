export default {
	async initialData({ commit }, payload) {
		let first_menu = payload.data_menu[0]
		if (first_menu.menu_child.length == 0) {
			commit("setCurrentMenuParentId", first_menu.menu_id)
			commit("setCurrentMenuId", first_menu.menu_id)
			commit("setDefaultMenu", first_menu.menu_url)
			commit("setAccessPage", first_menu.menu_access)
		} else {
			commit("setCurrentMenuParentId", first_menu.menu_id)
			commit("setCurrentMenuId", first_menu.menu_child[0].menu_id)
			commit("setDefaultMenu", first_menu.menu_child[0].menu_url)
			commit("setAccessPage", first_menu.menu_child[0].menu_access)
		}
		commit("setDataUser", payload.data_user)
		commit("setDataMenus", payload.data_menu)
		commit("setAccessToken", payload.access_token)
	},
}
