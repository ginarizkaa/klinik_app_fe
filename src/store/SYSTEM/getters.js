export default {
	getDataUser: (state) => state.dataUser,
	getDataMenus: (state) => state.dataMenus,
	getAccessPage: (state) => state.accessPage,
	getAccessToken: (state) => state.accessToken || "",
	getDefaultMenu: (state) => state.defaultMenu,
	getCurrentMenuId: (state) => state.currentMenuId,
	getCurrentMenuParentId: (state) => state.currentMenuParentId,
}
